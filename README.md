#PASOS A SEGUIR LA CHALLENGE EN LA CREACION DE LA BASE DE DATOS

cd mysql-challenge/

docker build -t mysql-challenge:1 .

docker images

docker ps -a

docker run -it -d -p 4000:3306 -e MYSQL_ROOT_PASSWORD=root --name mydb mysql-challenge:1 mysqld --lower_case_table_names=1
