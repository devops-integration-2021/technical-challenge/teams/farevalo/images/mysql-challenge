FROM mysql:5.6

MAINTAINER Fabian Arevalo <farevalo.farevalo@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d
